const axios = require("axios");

async function get_stock() {
    var result = [];

    await axios.get(process.env.URL_BACK_OFFICE_MAGASIN + '/store-stock-position').then(function (response) {
        result.push(response.data);
    }).catch((err) => {
        result.push(err.data)
    });

    return result
}

module.exports.get_stock = get_stock;