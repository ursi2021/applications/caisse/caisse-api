const axios = require("axios");

async function get_sales() {
    var result = [];
    await axios.get(process.env.URL_BACK_OFFICE_MAGASIN + '/products/store').then(function (response) {
        //await axios.get(process.env.URL_REFERENTIEL_PRODUIT + '/products/store').then(function (response) {
        result.push(response.data);
    }).catch((err) => {
        result.push(err.data)
    });

    return result
}

async function get_assortments() {
    var result = []
    await axios.get(process.env.URL_REFERENTIEL_PRODUIT + '/products').then(function (response) {
        result.push(response.data);
        console.log(response.data);
    }).catch((err) => {
        result.push(err.data)
    });
    return result
}

async function get_sales_clock() {
    var result = []
    var url = "https://localhost:8443/caisse/clock-register"
    await axios.get(url).then(function (response) {
        result.push(response.data);
        console.log(response.data);
    }).catch((err) => {
        result.push(err.data)
    });
    return result 
}

module.exports.get_sales = get_sales;
module.exports.get_assortments = get_assortments;
module.exports.get_sales_clock = get_sales_clock;