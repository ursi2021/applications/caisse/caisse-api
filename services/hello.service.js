const axios = require("axios");

async function getHelloApps() {
    let result = []
    let applications = [
        {appli:'Back office magasin', err: false, url: process.env.URL_BACK_OFFICE_MAGASIN},
        {appli:'Caisse', err: false, url: process.env.URL_CAISSE},
        {appli:'Décisionnel', err: false, url: process.env.URL_DECISIONNEL},
        {appli:'E-commerce', err: false, url: process.env.URL_E_COMMERCE},
        {appli:'Gestion commerciale', err: false, url: process.env.URL_GESTION_COMMERCIALE},
        {appli:'Gestion entrepôts', err: false, url: process.env.URL_ENTREPROTS},
        {appli:'Gestion promotion', err: false, url: process.env.URL_GESTION_PROMOTION},
        {appli:'Monétique paiement', err: false, url: process.env.URL_MONETIQUE_PAIEMENT},
        {appli:'Référentiel produit', err: false, url: process.env.URL_REFERENTIEL_PRODUIT},
        {appli:'Relation client', err: false, url: process.env.URL_RELATION_CLIENT}
    ];
    for (let i = 0; i < applications.length; i++) {
        await axios.get(applications[i].url + '/hello' ).then(function (response) {
            result.push({application: applications[i].appli, response: JSON.stringify(response.data)});
        }).catch(() => {
            applications[i].err = true;
            result.push({application: applications[i].appli, error: 'Error to fetch data'});
        });
    }
    return result
}

async function getrefprod() {
    let result = []
    await axios.get(process.env.URL_REFERENTIEL_PRODUIT + '/products').then(function (response) {
        result.push(response.data);
    }).catch((err) => {
        result.push(err.data)
    });
    return result
}

async function getinfoclient() {
    var result = [];
    var error = false;

    await axios.get(process.env.URL_RELATION_CLIENT + '/clients').then(function (response) {
        result.push(response.data);
    }).catch((err) => {
        result.push(err.data)
    });
    return result
}

module.exports.getHelloApps = getHelloApps;
module.exports.getrefprof = getrefprod;
module.exports.getinfoclient = getinfoclient;

