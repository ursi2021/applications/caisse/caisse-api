var chai = require('chai');
chai.use(require('chai-http'));
const app = require("../app");
var assert = require('assert');
const { doesNotMatch } = require('assert');
const { expect } = chai;
var template1 = require('../controller/stock_controller')(app);
var template2 = require('../controller/sales_controller')(app);

describe('Stock_controller', function () {
  describe('#indexOf()', function (req, res, next) {
    it('Stock service', function () {
     var result = template1.stock_get();
     assert.notEqual(result, null);
    });
  });
});

describe('Sales_controller', function () {
  describe('#indexOf()', function (req, res, next) {
    it('Sales table is not empty', function () {
     var sales = template2.sales();
     assert.notEqual(sales, null);
    });
  });
});

describe('Sales_controller', function () {
  describe('#indexOf()', function (req, res, next) {
    it('store_sales_get', function () {
     var result = template2.store_sales_get();
     assert.notEqual(result, null);
    });
  });
});

describe('Sales_controller', function () {
  describe('#indexOf()', function (req, res, next) {
    it('payment_get', function () {
     var result = template2.payment_get();
     assert.notEqual(result, null);
    });
  });
});
