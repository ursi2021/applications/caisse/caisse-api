var controller = {}, _app = {};
var logger = require('../logs');

module.exports = function(app) {
_app = app
return controller
}

//MODELS VARS
const models = require("../models/index");
var User = models.User;


/**@swagger
 * /users:
 *   get:
 *     tags:
 *       - users
 *     summary: get users page
 *     responses:
 *       200:
 *         description: Return users html in user view
 */
controller.user = function(req, res, next) {
    User.findAll().then(function(users) {
        logger.info(users);
        res.render('users', {title: 'Users', users: users});
    });
};

/**@swagger
 * /api/user:
 *   get:
 *     tags:
 *       - Users
 *     summary: get all users
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of users
 *         schema:
 *           type: array
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Failed to retrieve data
 */
controller.get_users = function(req, res, next) {
    try {
        User.findAll().then(user => res.status(200).json(user));
    } catch (e) {
        res.status(404).json({'error': 'FindAll'});
    }
};

/**@swagger
 * /api/user:
 *   post:
 *     summary: create user
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: Return created user
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Failed to create data
 */
controller.create_user = function(req, res, next) {
    try {
        logger.info(req.body);
        User.create(req.body).then(function (user) {
        res.status(200).json(user);
        });
    } catch (e) {
        res.status(404);
    }
};