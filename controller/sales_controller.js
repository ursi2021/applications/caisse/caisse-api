var controller = {}, _app = {};
var logger = require('../logs');
var axios = require('axios');
const { Sequelize } = require('sequelize');
const sales_services =  require('../services/sales.service')

module.exports = function(app) {
    _app = app
    return controller
}

//MODELS VARS
const models = require("../models/index");
var Sale = models.Sale;
var Product = models.Product;
var Stock = models.Stock;
//var Payment = models.Payment;
var my_id;
var my_amount;
var my_card_number;
var my_validity_date;
var my_ccv;
var my_last_name;
var my_first_name;
var my_provider;

var my_paid = ""

var data = {};

controller.clock_sales = async function(req, res, next) {
    const params = req.body
	const clock_sales = await sales_services.clock_sales(params)
    res.status(200).json(params)
}
/**@swagger
 * /sales:
 *   get:
 *     tags:
 *       - Sales
 *     summary: get products page for the client to buy from DB or ref-products
 *     responses:
 *       200:
 *         description: Return sale html
 */
controller.sales = async function(req, res, next) {
    //var result = await sales_services.get_sales_clock();
    var result = await sales_services.get_sales();
    //add Stock in database if doesnt exist
    var store = {
        storeId: 1,
        stockQuantity: 20,
        createdAt: '2020-11-01 00:00:00'
    };
    await Stock
      .findOne({where: {storeId: 1}})
      .then(function(obj) {
          if(!obj)
              Stock.create(store);
      }).catch(e => {
          console.log(e);
      });
    //add products in 
    for (let elm of result) {
        if (elm) {
            for (let e of elm) {
                let price = 20;
                if (e.price != 0)
                    price = e.price;
            var product = {
                    code: e['product-code'],
                    family: e['product-family'],
                    description: e['product-description'],
                    minQuantity: e['min-quantity'],
                    packaging: e['packaging'],
                    price: e.price,
                    storeId: 1
                }
                await Product
                .findOne({where: {code: e['product-code']}})
                .then(function(obj) {
                if(!obj) 
                    Product.create(product);
                })
            }
        }
      }
    await Product.findAll().then(function(product) {
      logger.info(product);
      res.render('sale', {title: 'Sales', product: product});
    });
};

controller.statuspayment = function (req, res, next){
    Sale.findAll({
        where: {
            saleNumber: my_id
        }
    }).then((sale) => {
        if (sale.length == 0)
            my_paid = "No sale with this id " + my_id;

        if (sale[0].paid == 0)
            my_paid = "Payment is in progress "

        if (sale[0].paid == 1)
            my_paid = "Payment is successful"

        if (sale[0].paid == 2)
            my_paid = "Payment is refused"
    })

    res.render('acceptpayment', {paid: my_paid});
    if (req.body.status == "yes"){
        helper();
    }
    else
        helper();
}

controller.statuspayment_get = function (req, res, next) {
    res.render('acceptpayment', {paid: my_paid});
}

function helper() {

    try {
        axios.get(process.env.URL_MONETIQUE_PAIEMENT + '/payments/' + my_id + '?from=caisse').then(function (response) {
            if (!response || !response.data || !response.data.status) {
                return;
            }
            if (response.data.status == 'accepted') {
                Sale.findAll({
                    where: {
                        paid: 0
                    }
                }).then((sales) => {
                    if (!sales) {
                        return;
                    }
                    for (s in sales) {
                        sales[s].update({
                            paid: 1
                        });
                    }
                    return;
                }).catch((err) => {
                    return;
                })
            }
            if (response.data.status == 'declined') {
                    Sale.findAll({
                        where: {
                            paid: 0
                        }
                    }).then((sales) => {
                        if (!sales) {
                            return;
                        }
                        for (s in sales) {
                            sales[s].update({
                                paid: 2
                            });
                        }
                        return;
                    }).catch((err) => {
                        return;
                    })
            } else {
                console.log("error");
            }
        })
    } catch (e) {
        console.log(e);
    }
}

/**@swagger
 * /caisse/store-sales:
 *   post:
 *     summary: create new sales
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Sale'
 *     responses:
 *       200:
 *         description: Return created sale
 *         schema:
 *           $ref: '#/definitions/Sale'
 *       404:
 *         description: Failed to create data
 */
controller.store_sales_post = function(req, res, next) {
    try {
        Sale.findOne({
            order: [ [ 'createdAt', 'DESC' ]],
            attributes: ['saleNumber', 'paid']
        }).then(function(d) {
            let newSalenumber = 1;
            if (d){
                if (d.paid) {
                    newSalenumber = d.saleNumber + 1;
                }
                else
                    newSalenumber = d.saleNumber;
            }
            data = {
                productCode : req.body.productCode,
                userId : 1,
                clientQuantity : req.body.clientQuantity, 
                saleNumber : newSalenumber,
            }
            logger.info(data);
            Sale.create(data).then(function (sale) {
            res.status(200);
            });
      });
    } catch (e) {
      res.status(404);
    }
};

/**@swagger
 * /caisse/store-sales:
 *   get:
 *     summary: get all sales of the day
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Sale'
 *     responses:
 *       200:
 *         description: Return created sale
 *         schema:
 *           $ref: '#/definitions/Sale'
 *       404:
 *         description: Failed to create data
 */
controller.store_sales_get = async function(req, res, next) {
    const Op = Sequelize.Op;
    const TODAY_START = new Date().setHours(0, 0, 0, 0);
    const NOW = new Date();
    await Sale.findAll({
      attributes : [["createdAt", 'date'], ['saleNumber', 'id'], ['productCode', 'code'], ['clientQuantity', 'quantity']],
      where: {
          createdAt: { 
          [Op.gt]: TODAY_START,
          [Op.lt]: NOW
          } ,
          paid: 1, 
      }
      }).then((sales) => { 
        let newsales = []
        for (s in sales) {
            tmp = {
                date: sales[s].dataValues.date,
                saleNumber: sales[s].dataValues.id,
                code: sales[s].dataValues.code,
                quantity: sales[s].dataValues.quantity,
            }
            newsales.push(tmp)
        }
        let sortedSale = newsales.reduce((r, a) => {
            r[a.saleNumber] = [...r[a.saleNumber] || [], a];
            return r;
           }, {});
        let js = new Map();
        for (p in sortedSale) {
            let tab = []
            for (k in sortedSale[p]) {
                let tmp = {
                    'product-code' : sortedSale[p][k].code,
                    quantity: sortedSale[p][k].quantity
                }
                tab.push(tmp)
            }
            js.set(p, tab);
        };
        let fin = []
        for (b in sortedSale) {
            let tmp = {
                date: sortedSale[b][0].date,
                'product-quantity-map': js.get(b)
            }
            fin.push(tmp);
        }
        res.status(200).json(fin)}
    ).catch((e) => {
        res.status(404).json({'error': 'FindAll : ' + e});
    })
};

controller.assortment = async function (req, res, next) {
  /*await axios.get(process.env.URL_BACK_OFFICE_MAGASIN + '/products/store').then(function (response) {
      result.push(response.data);
  }).catch((err) => {
      result.push(err.data)
  });

  try {
      var mystring = "Assortiment magasin: ";
      for (const elm of result)
          mystring = mystring + JSON.stringify(elm) + ' \n ';
      res.status(200).render('product',{products: mystring});
      console.log('My products', mystring);
  } catch(err){
      result.push(err.data);
      console.log('ERROR', result);
  }*/

  var result = await sales_services.get_assortments();

  //add Stock in database if doesnt exist
  var store = {
      storeId: 1,
      stockQuantity: 20,
      createdAt: '2020-11-01 00:00:00'
  };
 await Stock
    .findOne({where: {storeId: 1}})
    .then(function(obj) {
        if(!obj)
            Stock.create(store);
    }).catch(e => {
        console.log(e);
    });
  for (elm of result) {
      for (e of elm) {
        let price = 20;
        if (e.price != 0)
            price = e.price;
        var product = {
              code: e['product-code'],
              family: e['product-family'],
              description: e['product-description'],
              minQuantity: e['min-quantity'],
              packaging: e['packaging'],
              price: price,
              storeId: 1
          }
          await Product
          .findOne({where: {code: e['product-code']}})
          .then(function(obj) {
            if(!obj) {
                Product.create(product);
            }}
            )
        }
    }
    res.status(200).json(result);
};


controller.payment = function (req, res, next){
    var data;
    var data2;
    let sum = req.body.sum;
    let cash = req.body.payments === "cash";
    if (cash) {
        Sale.findAll({
            where: {
                paid: 0
            }
        }).then((sales) => {
            if(!sales) { 
                res.status(400);
                return;
            } 
            for (s in sales) {
                sales[s].update({
                    paid: 1
                });
            }
            my_paid = "Paid with cash is done";
            res.status(200);
            return;
        }).catch((err) => {
            res.status(400)
            return;
        })
    }
    if (!cash) {

        Sale.findOne({
            order: [ [ 'createdAt', 'DESC' ]],
            attributes: ['saleNumber', 'paid']
        }).then(function(d) {
            if (! d.saleNumber){
                res.status(400);
                return;
            }
            let newSalenumber = d.saleNumber;

        // envoyer les infos
        my_id = newSalenumber;
        my_amount = sum;
        my_card_number = parseInt(req.body.cardNumber);
        my_validity_date = req.body.validityDate;
        my_ccv = parseInt(req.body.ccv);
        my_last_name = req.body.lastName;
        my_first_name = req.body.firstName;
        my_provider = req.body.bankProvider;

        data = {
            id : newSalenumber,
            amount: sum, 
            from: 'caisse',
            'card-number': parseInt(req.body.cardNumber),
            'validity-date': req.body.validityDate,
            'ccv': parseInt(req.body.ccv),
            'last-name': req.body.lastName,
            'first-name': req.body.firstName,
            'provider': req.body.bankProvider,
            account: "Account1"
        };
        let url = process.env.URL_MONETIQUE_PAIEMENT + "/payment/";
        axios.post(url, data).then((response) => {
            if (!response || !response.data || !response.data.status) {
                console.log("No response -----------------------------------------")
                return;
            }
            console.log("FRONT RESPONSE")
            console.log(response.data.status)
            if (response.data.status == 'accepted'){
                
                console.log("ACCEPTED -----------------------------------------")

                Sale.findAll({
                    where: {
                        paid: 0
                    }
                }).then((sales) => {
                    if(!sales) {
                        return;
                    }
                    for (let s= 0; s < sales.length; s++) {
                        sales[s].update({
                            paid: 1,
                            paymentMean: 1
                        });
                    }
                    return;
                }).catch((err) => {
                    return;
                })
            }
            if (response.data.status == 'declined'){
                Sale.findAll({
                    where: {
                        paid: 0
                    }
                }).then((sales) => {
                    if(!sales) {
                        return;
                    }
                    for (let s= 0; s < sales.length; s++) {
                        sales[s].update({
                            paid: 2,
                            paymentMean: 1
                        });
                    }
                    return;
                }).catch((err) => {
                    return;
                })
            }
        }).catch((err) => {
            console.log(err)
        });
        //res.status(200).redirect('/caisse/caisse/payment/status');
        }).catch((err) => {
            console.log(err)
        });
        return;
    }
};



controller.payment_get = async function(req, res){
    try {
        res.status(200).json({
        id : my_id, //FIXME add the payment id
        amount: my_amount, //FIXED 
        'card-number': my_card_number,
        'validity-date': my_validity_date,
        'ccv': my_ccv,
        'last-name': my_last_name,
        'first-name': my_first_name,
        'provider': my_provider
        })

        res.status(200);

        axios.get(process.env.URL_MONETIQUE_PAIEMENT + '/payments/' + my_id + '?from=caisse').then(function (response) {
            if (!response || !response.data || !response.data.status) {
                return;
            }
            if (response.data.status == 'accepted'){
                Sale.findAll({
                    where: {
                        paid: 0
                    }
                }).then((sales) => {
                    if(!sales) {
                        res.status(400);
                        return;
                    }
                    for (s in sales) {
                        sales[s].update({
                            paid: 1
                        });
                    }
                    res.status(200);
                    return;
                }).catch((err) => {
                    res.status(400)
                    return;
                })
            }
            else {
                this.payment();
                res.status(400);
            }})
    } catch (e) {
        log(e);
        res.status(404);
    }
    res.end();
}

controller.store_sales_totalCount_get = function(req, res, next) {
    try {
        const Op = Sequelize.Op;
        const DATE = new Date(Date.now() - (60 * 60 * 1000));
        Sale.findAll({
        where : {
            userId : 1,
            createdAt: {
                [Op.lt]: DATE,
              }
        },
        attributes : [["createdAt", 'date'],"clientQuantity"],
        include: {
            model : Product,
            attributes : ["price"]
        }
        }).then(sales => {
            var total = 0;
            var salesSize = sales.length;
            for (i = 0; i < salesSize; i++){
                let sale = sales[i];
                total += (sale.Product.price * sale.clientQuantity);
            }
            res.status(200).json({'Data': 'Total amount = ' + total});
          })
    } catch (e) {
        res.status(404).json({'error': 'FindAll : ' + e});
    }
}

controller.json_sales = function (req, res, next) {
    try {

    } catch(e) {

    } 
}
