var controller = {}, _app = {};
var axios = require('axios');
var logger = require('../logs');
const { Sequelize } = require('sequelize');
var stock_services = require('../services/stock.services')
module.exports = function(app) {
    _app = app
    return controller
}

//MODELS VARS
/*
const models = require("../models/index");
var Stock = models.Stock;
var Product = models.Product;*/


/**@swagger
 * /store-stock-position:
 *   get:
 *     tags:
 *       - Store Stocks
 *     summary: get store stocks from BO
 *     responses:
 *       200:
 *         description: Return stock html
 */
controller.stock_get = async function(req, res, next) {
    var result = await stock_services.get_stock()

    try {
        res.render('stock',{stocks: result[0][0]['product-quantity-map'], title: 'Store Stocks'});
    } catch(err){
        result.push(err.data);
    }
}