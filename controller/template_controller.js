var controller = {}, _app = {};
var axios = require('axios');
const e = require('express');
const { Sequelize } = require('sequelize/');
const helloServices =  require('../services/hello.service')

module.exports = function(app) {
    _app = app
    return controller
}


//MODELS VARS
const models = require("../models/index");
var Product = models.Product;
var Stock = models.Stock;


controller.index = function(req, res, next) {
    res.render('index', { title: 'Express' });
}

controller.hello = function(req, res, next) {
    res.json({caisse : 'Hello World !'});
    res.end();
}



controller.helloall = async function(req, res, next) {
    const result = await helloServices.getHelloApps();
    var mystring = "";
    for (const elm of result)
        mystring = mystring + JSON.stringify(elm) + ' -- ';
    res.render('hello', {hellos: mystring});
};

controller.ref_produit = async function (req, res, next) {
    var result = await helloServices.getrefprof();

    try {
        var mystring = "";
        for (const elm of result)
            mystring = mystring + JSON.stringify(elm) + ' -- ';

        res.render('product',{products: mystring});
    } catch(err){
        result.push(err.data);
    }
};

controller.info_client = async function (req, res, next) {
    var result = helloServices.getinfoclient();

    try {
        var mystring = "Clients: ";
        for (const elm of result)
            mystring = mystring + JSON.stringify(elm) + ' -- ';

        res.render('client',{clients: mystring});
    } catch(err){
        result.push(err.data);
    }
};
