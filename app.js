var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var swaggerJSDoc = require('swagger-jsdoc');
//var kong_service = require('services/kong.service')
var axios = require('axios')


const swaggerUi = require('swagger-ui-express');
require('dotenv-flow').config({path:'config/'});


var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const options = {
  definition: {
    swagger: '2.0', // Specification (optional, defaults to swagger: '2.0')
    info: {
      title: process.env.APP_NAME, // Title (required)
      version: '1.0.0', // Version (required)
    },
    servers : [{"url" : "/", "description" : "Local dev"}]
    },

  // Path to the API docs
  apis: ['models/*.js', 'routes/*.js', 'controller/*.js'],
};
const swaggerSpec = swaggerJSDoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

/* ___________________________________________________________ */
/////////////REGISTER TO KONG/////////////////

var registered = false;

function register_kong() {
  axios
      .post('http://kong:8081/services/', {
        name: process.env.APP_NAME,
        url: 'http://' + process.env.APP_NAME
      })
      .then(res => {
        console.log(`statusCode: ${res.statusCode}`)
        console.log(res)
      })
      .catch(error => {
        console.error(error)
        return false;
      })

  axios
      .post('http://kong:8081/services/' + process.env.APP_NAME + '/routes', {
        paths: ["/" + process.env.APP_NAME],
        name: process.env.APP_NAME
      })
      .then(res => {
        console.log(`statusCode: ${res.statusCode}`)
        console.log(res)
      })
      .catch(error => {
        console.error(error)
        return false;
      })
  return true;
}

while (registered === false) {
  registered = register_kong();
}

/* ___________________________________________________________ */

////////REGISTER TO CLOCK ////////////
const clockRouter = require("./controller/clock_controller"); 
app.use("/clock-register", clockRouter);

/* ___________________________________________________________ */


// define a path to a route file
// ex : var <route varname> = require('<relative path>')

var indexRouter = require('./routes/index');
var helloRouter = require('./routes/hello');
var helloAllRouter = require('./routes/hello');

// Create a redirection
// ex : app.use('<redirection>', <route varname>)

app.use('/', indexRouter);
app.use('/caisse/hello', helloRouter);
app.use('/caisse/hello/all', helloAllRouter);

/**********************************************************************/
/**********************************************************************/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

