'use strict';
const { DataTypes} = require('sequelize');
/**
 * @swagger
 * definitions:
 *  Sale:
 *      type: object
 *      properties:
 *          productCode:
 *              type: string
 *          userId:
 *              type: int
 *          clientQuantity:
 *              type: int
 *          paymentMean:
 *              type: boolean
 *          createdAt:
 *              type: date
 */
module.exports = function(sequelize) {
  const Sale = sequelize.define('Sale', {
    // Model attributes are defined here
    productCode: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    clientQuantity: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    paymentMean : {
      type: DataTypes.BOOLEAN,
      defaultValue: false //0 for Cash, 1 for Credit Card
    }, 
    saleNumber : {
      type: DataTypes.INTEGER
    },
    paid : {
      type: DataTypes.INTEGER,
      defaultValue: 0 //0 for Notpaid, 1 for paid
    }
  }, {
    timestamps: true, 
    updatedAt: false
    // Other model options go here
  });
  return Sale
};
