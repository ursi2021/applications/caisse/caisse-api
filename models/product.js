'use strict';
const {DataTypes} = require('sequelize');
/**
 * @swagger
 * definitions:
 *  Product:
 *      type: object
 *      properties:
 *          code:
 *              type: string
 *          family:
 *              type: string
 *          desciption:
 *              type: string
 *          minQuantity:
 *              type: int
 *          packaging:
 *              type: int
 *          price: 
 *              type: int
 *          storeId:
 *              type: int
 */
module.exports = function(sequelize) {
  const Product = sequelize.define('Product', {
    // Model attributes are defined here
    code: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    family: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.STRING,
    },
    minQuantity: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    packaging: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    storeId: { //foreignKey to stock
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    timestamps: false
    // Other model options go here
  });

  return Product;
};
