'use strict';
const {DataTypes} = require('sequelize');
/**
 * @swagger
 * definitions:
 *  Stock:
 *      type: object
 *      properties:
 *          storeId:
 *              type: integer
 *          storeQuantity:
 *              type: int
 */
module.exports = function(sequelize) {
  const Stock = sequelize.define('Stock', {
    // Model attributes are defined here
    storeId: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    stockQuantity: {
        type: DataTypes.INTEGER,
        defaultValue: 0 
    },
    createdAt : {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()')
    }
  }, {
        updatedAt: false
        // Other model options go here
  });

  return Stock;
};