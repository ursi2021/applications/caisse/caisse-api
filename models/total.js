'use strict';
const { DataTypes} = require('sequelize');
/**
 * @swagger
 * definitions:
 *  Total:
 *      type: object
 *      properties:
 *          productCode:
 *              type: string
 *          userId:
 *              type: int
 *          clientQuantity:
 *              type: int
 *          paymentMean:
 *              type: boolean
 *          createdAt:
 *              type: date
 */
module.exports = function(sequelize) {
  const Total = sequelize.define('Total', {
    // Model attributes are defined here

    saleNumber : {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    price : {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    account : {
      type: DataTypes.STRING,
      defaultValue: null
    }
  }, {
    timestamps: false, 
    updatedAt: false
    // Other model options go here
  });
  return Total
};
