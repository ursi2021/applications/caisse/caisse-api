'use strict';

const {Sequelize} = require('sequelize');
const logger = require('../logs');
var models = {};
module.exports = models;

const fs = require('fs');
var axios = require('axios');

// login to mariadb database
// ex : new  new Sequelize(<dbname>, <user>, <password>, ...);

initialize()
async function initialize() {
  
  const sequelize = new Sequelize(process.env.URSI_DB_NAME, process.env.URSI_DB_USER,process.env.URSI_DB_PASSWORD, {
    host: process.env.URSI_DB_HOST,
    port: Number(process.env.URSI_DB_PORT),
    logging: msg => logger.verbose(msg),
    dialectOptions: {
      timezone: 'Etc/GMT0'
    },
    dialect: 'mariadb'
  });

  sequelize.authenticate().then(() => {
    logger.info('Connection has been established successfully.');
  }).catch (error => {
    logger.error('Unable to connect to the database:', error);
  });

  // Init models
  models.Payment = require('./payment')(sequelize);
  models.Product = require('./product')(sequelize);
  models.Sale = require('./sale')(sequelize);
  models.Stock = require('./stock')(sequelize);
  models.User = require('./user_template')(sequelize);
  models.Total = require('./total')(sequelize);

  //associations ---> TO BE FIXED --->
  models.Sale.belongsTo(models.Product, {foreignKey: 'productCode', targetKey: 'code'});
  models.Product.hasMany(models.Sale, {foreignKey: 'productCode', sourceKey: 'code'});
  
  models.Sale.belongsTo(models.User, {foreignKey: 'userId', targetKey: 'id'});
  models.User.hasOne(models.Sale, {foreignKey: 'userId', sourceKey: 'id'});
  
  models.Payment.belongsTo(models.User, {foreignKey: 'userId', targetKey: 'id'});
  models.User.hasOne(models.Payment, {foreignKey: 'userId', sourceKey: 'id'});

  models.Product.belongsTo(models.Stock, {foreignKey: 'storeId', targetKey: 'storeId'});
  models.Stock.hasOne(models.Product, {foreignKey: 'storeId', sourceKey: 'storeId'});
  

  await sequelize.sync({force: true});
  logger.info("The table for the Payment model was just (re)created!");
  logger.info("The table for the User model was just (re)created!");
  logger.info("The table for the Sale model was just (re)created!");
  logger.info("The table for the Product model was just (re)created!");
  logger.info("The table for the Stock model was just (re)created!");
  logger.info("The table for the Total model was just (re)created!");
  setTimeout( () => {
    getJsonSales();
  }, 10000);
  
}

/////////////////////////////////////////// GET SALES FROM JSON FILE ////////////////////////////////////////////////

async function getJsonSales() {
  
  try {
    //add Stock
    await models.Stock.findOne({where: {storeId: 1}}).then(function(obj) {
          if(!obj){
            var store = {
              storeId: 1,
              stockQuantity: 20,
              createdAt: '2020-11-01 00:00:00'
            };
            models.Stock.create(store).then().catch(e => {console.log(e);});
          }}).catch(e => {console.log(e);});
    
    //add User
    await models.User.findOne({where: {id: 1}}).then(function(obj) {
      if(!obj){
        var user = {
          id: 1,
          firstName: 'john',
          lastName: 'doe'
        };
        models.User.create(user).then().catch(e => {console.log(e);});
      }}).catch(e => {console.log(e);});
    
    //get products from ref produits and add them to the DB
    let result = [];
    await axios.get(process.env.URL_BACK_OFFICE_MAGASIN + '/products/store').then(function (response) {
    //await axios.get(process.env.URL_REFERENTIEL_PRODUIT + '/products/store').then(function (response) {
      result.push(response.data);
    }).catch((err) => {
        result.push(err.data)
    });
    for (let elm of result) {
      if (elm) {
          for (let e of elm) {
          var product = {
                  code: e['product-code'],
                  family: e['product-family'],
                  description: e['product-description'],
                  minQuantity: e['min-quantity'],
                  packaging: e['packaging'],
                  price: e.price,
                  storeId: 1
              }
              await models.Product
              .findOne({where: {code: e['product-code']}})
              .then(function(obj) {
              if(!obj) 
                  models.Product.create(product).then().catch(e => {console.log(e);});;
              })
          }
      }
    }
    logger.info('got products from ref prod and added them to the DB')

    //parse raw data
    const data = fs.readFileSync('./models/sales.json');
    const insert = JSON.parse(data);
    for (let t in insert.tickets) { ///début for 1
      ////////////////////////////////////////CASH/////////////////////////////////////
      if (insert.tickets[t].modePaiement == 'CASH') {
          for (let p in insert.tickets[t].panier) {
              //add Sale in DB
              await models.Product
              .findOne({where: {code: insert.tickets[t].panier[p].codeProduit}})
              .then(async function(obj) {
              if(obj) {
                let sale = {
                  productCode: insert.tickets[t].panier[p].codeProduit,
                  userId: 1,
                  clientQuantity: insert.tickets[t].panier[p].quantity,
                  paymentMean: 0,
                  saleNumber: t,
                  paid: 1,
                  createdAt: new Date(),
                }
                await models.Sale.create(sale).then(logger.info('sale added')).catch(e => {console.log(e);});
              }        
            })  
        }
        await models.Sale.findAll({
          where: {
            saleNumber: t
          },
          attributes : ['productCode', 'clientQuantity']
        }).then(async (data2) => {
          let total = 0;
          let products = [];
          for (let i of data2) {
            await models.Product.findOne({
              where: {code: i.productCode}
            }).then((prod) => {
              total += prod.price * i.clientQuantity
            })
            products.push(i.dataValues)
          }
          console.log("Total End -------------------------------")
          console.log(total)
          let toto = {
            saleNumber: t,
            price: total,
            account: insert.tickets[t].account,
          }
          models.Total.create(toto).then(logger.info('total added')).catch(e => {console.log(e);});
          //IF PAYMENT CASH && ACCOUNT -> SEND SALE TO RC
          if (insert.tickets[t].account != undefined) {
            console.log(insert.tickets[t].account)
            let url = process.env.URL_RELATION_CLIENT + "/data/web-sales"
            let send_sale = {
              "sum-price": total,
              account: insert.tickets[t].account,
              id : parseInt(t),
             'product-quantity-map': products,
            }
             axios.post(url, send_sale).then((response) => {
              console.log(response.data);
            }).catch((err) => {
              console.log(err)
              return;})

          }
        })}
        
        /////////////////////////////////////////////////////////CB////////////////////////////////////////////////
        if (insert.tickets[t].modePaiement == 'CARD') {
          if (insert.tickets[t].account == undefined) {continue;} ////si CB + pas d'account => paiement refusé

          for (let p in insert.tickets[t].panier) {
              //add Sale in DB
              await models.Product
              .findOne({where: {code: insert.tickets[t].panier[p].codeProduit}})
              .then(async function(obj) {
              if(obj) {
                let sale = {
                  productCode: insert.tickets[t].panier[p].codeProduit,
                  userId: 1,
                  clientQuantity: insert.tickets[t].panier[p].quantity,
                  paymentMean: 1,
                  saleNumber: t,
                  paid: 0,
                  createdAt: new Date(),
                }
                await models.Sale.create(sale).then(logger.info('sale added')).catch(e => {console.log(e);});
              }        
            })  
        }
        await models.Sale.findAll({
          where: {
            saleNumber: t
          },
          attributes : ['productCode', 'clientQuantity']
        }).then(async (data2) => {
          let total = 0;
          let products = [];
          for (let i of data2) {
            await models.Product.findOne({
              where: {code: i.productCode}
            }).then((prod) => {
              total += (prod.price * i.clientQuantity)
            })
            products.push(i.dataValues)
          }
          
          let toto = {
            saleNumber: t,
            price: total,
            account: insert.tickets[t].account,
          }
          models.Total.create(toto).then(logger.info('total added')).catch(e => {console.log(e);});
          // Contacter  MP / Attendre validation / Envoyer à RC
          // Account 
            let url = process.env.URL_MONETIQUE_PAIEMENT + "/payment/";
            axios.post(url, {
              from: 'caisse',
              id: toto.saleNumber,
              amount: toto.price,
              account: toto.account
            }).then((response) => {
            console.log('MP RESPONSE = --------------------');
            console.log(response.data);
              if (!response || !response.data || !response.data.status || (response.data.status != 'accepted' && response.data.status != 'declined')){
                console.log("No response -----------------------------------------")
                return;
              }
              if (response.data.status == 'accepted'){
                
                console.log("ACCEPTED -----------------------------------------")

                models.Sale.findAll({
                    where: {
                        saleNumber: t
                    }
                }).then((sales) => {
                    if(!sales) {
                        return;
                    }
                    for (let i=0; i < sales.length; i++) {
                    sales[i].update({
                            paid: 1
                        });
                    }
                    let url = process.env.URL_RELATION_CLIENT + "/data/web-sales"
                    let send_sale = {
                      "sum-price": total,
                      account: insert.tickets[t].account,
                      id : parseInt(t),
                     'product-quantity-map': products,
                    }
                     axios.post(url, send_sale).then((response) => {
                      console.log(response.data);
                    }).catch((err) => {
                      console.log(err)
                      return;})
                    return;
                }).catch((err) => {
                    return;
                })
            }
                if (response.data.status == 'declined'){
                    models.Sale.findAll({
                        where: {
                            paid: 0
                        }
                    }).then((sales) => {
                        if(!sales) {
                            return;
                        }
                        for (let i=0; i < sales.length; i++) {
                          sales[i].update({
                                  paid: 2
                              });
                          }
                        return;
                    }).catch((err) => {
                        return;
                    })
                }
            });
           }, (error) => { console.log('ERROR'); console.error(error)});
        }
    } /////fin for 1
    logger.info("Sale raw data has been added to the Database");


  } catch (e) {
    logger.error('Error while loading sales raw data', e);
  }
}