'use strict';
const { DataTypes} = require('sequelize');
/**
 * @swagger
 * definitions:
 *  Payment:
 *      type: object
 *      properties:
 *          userId:
 *              type: int
 *          saleId:
 *              type: int
 *          amount:
 *              type: float
 *          cardNumber:
 *              type: int
 *          validityDate:
 *              type: date
 *          ccv:
 *              type: int 
 *          provider:
 *              type: string
 */
module.exports = function(sequelize) {
  const Payment = sequelize.define('Payment', {
    // Model attributes are defined here
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    amount: {
      type: DataTypes.FLOAT,
    },
    saleNumber: {
      type: DataTypes.INTEGER
    },
    cardNumber: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    validityDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ccv: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    provider: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: true, 
    updatedAt: false
    // Other model options go here
  });
  return Payment;
};