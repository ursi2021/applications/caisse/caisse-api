var express = require('express');
var router = express.Router();
var app = express();
var template = require('../controller/template_controller')(app);

/* GET helloworld. */
router.get('/caisse/hello', template.hello);
router.get('/caisse/hello/all', template.helloall);
router.get('/caisse/clients', template.info_client);
router.get('/caisse/products', template.ref_produit);

module.exports = router;