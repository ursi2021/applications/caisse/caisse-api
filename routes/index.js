var express = require('express');
var router = express.Router();
var app = express();

//controllers
var template = require('../controller/template_controller')(app);
var template2 = require('../controller/sales_controller')(app);
var template3 = require('../controller/user_controller')(app);
var template4 = require('../controller/stock_controller')(app);


/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/

//HELLO
router.get('/', template.index)
router.get('/caisse/hello', template.hello);
router.get('/caisse/hello/all', template.helloall);
router.get('/caisse/clients', template.info_client);
router.get('/caisse/produits', template.ref_produit);

//SALES
router.get('/caisse/sales', template2.sales);
router.post('/caisse/store-sales', template2.store_sales_post);
router.get('/caisse/store-sales/totalCount', template2.store_sales_totalCount_get);
router.get('/caisse/store-sales', template2.store_sales_get);
router.get('/caisse/assortment', template2.assortment);
router.get('/caisse/payment', template2.payment_get);
router.post('/caisse/payment', template2.payment);
router.post('/caisse/payment/status', template2.statuspayment);
router.get('/caisse/payment/status', template2.statuspayment_get);

router.post('/caisse/clock-assortment', template2.clock_sales);

//USER
router.post('/caisse/user', template3.create_user);
router.get('/users', template3.user);
router.get('/caisse/user', template3.get_users);

//STOCK
router.get('/caisse/store-stock-position', template4.stock_get); //à récupérer à 8h avec la clock
//router.post('/store-stock-position', template4.stock_post);
router.get('/store_sales',  template2.json_sales);

module.exports = router;